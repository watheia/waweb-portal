import "./PrincipalNav.module.css"

/* eslint-disable-next-line */
export interface PrincipalNavProps {}

export function PrincipalNav(props: PrincipalNavProps) {
  return (
    <div>
      <h1>Welcome to PrincipalNav!</h1>
    </div>
  )
}

export default PrincipalNav
